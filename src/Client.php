<?php

namespace Alunalun;

use GuzzleHttp\Client as GuzzleClient;

class Client
{
    
    protected $clientId;

    protected $clientSecret;

    protected $callback;

    /**
     * Alunalun initialization.
     *
     * @param string     $clientId      the Client ID you have in your admin interface
     * @param string     $clientSecret  the Secret Key you have in your admin interface
     *
     */

    public function __construct($clientId, $clientSecret, $callback){
        
        $this->clientId = $clientId;
        $this->clientSecret = $clientSecret;
        $this->callback = $callback;

    }

    public function getCode()
    {
        $query = http_build_query([
            'client_id' => $this->clientId,
            'redirect_uri' => $this->callback,
            'response_type' => 'code',
            'scope' => '',
        ]);

        return "https://connect.alunalun.id/oauth/authorize?".$query;
    }

    public function authorizationCode()
    {
        $http = new GuzzleClient;

        $response = $http->post('https://connect.alunalun.id/oauth/token', [
            'form_params' => [
                'grant_type' => 'authorization_code',
                'client_id' => $this->clientId,
                'client_secret' => $this->clientSecret,
                'redirect_uri' => $this->callback,
                'code' => $_GET['code'],
            ],
        ]);

        $body = $response->getBody();
        $body = json_decode($body);

        return $body;
    }

    public function refreshToken($token)
    {
        $http = new GuzzleClient;

        $response = $http->post('https://connect.alunalun.id/oauth/token', [
            'form_params' => [
                'grant_type' => 'refresh_token',
                'refresh_token' => $token,
                'client_id' => $this->clientId,
                'client_secret' => $this->clientSecret,
                'scope' => '',
            ],
        ]);

        $body = $response->getBody();
        $body = json_decode($body);

        return $body;
    }

}